package myPackage;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
@Path("/ksiega")
public class CzerwonaKsiegaManager {
	
	@PersistenceContext
	static EntityManager em;
	
	public static Map<Integer, CzerwonaKsiega> storage = new HashMap<Integer, CzerwonaKsiega>();
	
	public CzerwonaKsiegaManager(){
		storage.put(1, new CzerwonaKsiega("EX","Wymarłe","Extinct"));
		storage.put(2, new CzerwonaKsiega("EW","Wymarłe na wolności","Extinct in the wild"));
		storage.put(3, new CzerwonaKsiega("CR","Krytycznie zagrożone","Critically endangered"));
		storage.put(4, new CzerwonaKsiega("EN","Zagrożone","Endangered"));
		storage.put(5, new CzerwonaKsiega("VU","Narażone","Vulnerable"));
		storage.put(6, new CzerwonaKsiega("NT","Bliskie zagrożenia","Near threatened"));
		storage.put(7, new CzerwonaKsiega("LC","Najmniejszej troski","Least concern"));
		storage.put(8, new CzerwonaKsiega("-","Nie objęte ochroną","Not protected"));
	}
	
	//METODY BAZUJĄCE NA "STORAGE":
	//cel: pierwsze kroki projektu, testowanie obsługi JSONów
	
	@GET
	@Path("/id/{ksiegaId}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getCzerwonaKsiegaById(@PathParam("ksiegaId") Integer id){
		CzerwonaKsiega ck = storage.get(id);
		if(ck == null){
			String returnInfo = "Nie znaleziono wpisu w księdze o podanym ID (" + id + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = makeJsonPretty(ck);
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/selectAllKsiega")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response selectAllKsiega(){
		String output = makeJsonPretty(storage);
		return Response.status(200).entity(output).build();
	}
	
	//DODATKOWE METODY:
	//cel: ładniejsze wyświetlanie JSONów
	
	public static String makeJsonPretty(Map<Integer, CzerwonaKsiega> myMap){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(myMap);
		return jsonOutput;
	}
	
	public static String makeJsonPretty(CzerwonaKsiega myKsiega){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(myKsiega);
		return jsonOutput;
	}
}
