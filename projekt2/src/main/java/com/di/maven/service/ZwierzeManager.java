package myPackage;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
@Path("/zwierze")
public class ZwierzeManager {

	@PersistenceContext
	static EntityManager em;
	
	private static Map<Integer, Zwierze> storage = new HashMap<Integer, Zwierze>();
	
	public ZwierzeManager(){
		storage.put(1, new Zwierze(1,"Żubr","Ryszard","VU",5));
		storage.put(2, new Zwierze(2,"Jeż","Karol","LC",7));
		storage.put(3, new Zwierze(3,"Bóbr","Eryk","LC",7));
		storage.put(4, new Zwierze(4,"Nietoperz","Janusz","LC",7));
		storage.put(5, new Zwierze(5,"Lis","Albert","LC",7));
		storage.put(6, new Zwierze(6,"Królik","Robert","NT",6));
		storage.put(7, new Zwierze(7,"Tur","Edward","EX",1));
	}
	
	//NullPointerException...
	@GET
	@Path("/select")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response selectZwierze(){
		Query query = em.createNamedQuery("zwierze.findId", Zwierze.class);
		query.setParameter("id",1);
		List<Zwierze> list = query.getResultList();
		
		Map<Integer, Zwierze> myMap = new HashMap<Integer, Zwierze>();
		for(Zwierze z:list){
			myMap.put(z.getId(),z);
		}
		String output = makeJsonPretty(myMap);
		return Response.status(200).entity(output).build();
	}
	
	//METODY BAZUJĄCE NA HSQL:
	
	public Zwierze getZwierzeByItsId(Integer id){
		Zwierze z = new Zwierze();
		try {	
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze WHERE id=" + id + ";");
			while(rs.next()){
				Integer id_new = Integer.parseInt(rs.getString(1));
				String gatunek = rs.getString(2);
				String imie = rs.getString(3);
				String ckgz = rs.getString(4);
				z.setId(id_new);
				z.setGatunek(gatunek);
				z.setImie(imie);
				z.setCKGZ(ckgz);
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		if(z.getId() == null) return new Zwierze();
		return z;
	}
	
	public Zwierze getZwierzeByItsGatunek(String gatunek){
		Zwierze z = new Zwierze();
		try {	
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze WHERE gatunek='" + gatunek + "';");
			while(rs.next()){
				Integer id = Integer.parseInt(rs.getString(1));
				String gatunek_new = rs.getString(2);
				String imie = rs.getString(3);
				String ckgz = rs.getString(4);
				z.setId(id);
				z.setGatunek(gatunek_new);
				z.setImie(imie);
				z.setCKGZ(ckgz);
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		if(z.getId() == null) return new Zwierze();
		return z;
	}
	
	public Zwierze getZwierzeByItsImie(String imie){
		Zwierze z = new Zwierze();
		try {	
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze WHERE imie='" + imie + "';");
			while(rs.next()){
				Integer id = Integer.parseInt(rs.getString(1));
				String gatunek = rs.getString(2);
				String imie_new = rs.getString(3);
				String ckgz = rs.getString(4);
				z.setId(id);
				z.setGatunek(gatunek);
				z.setImie(imie_new);
				z.setCKGZ(ckgz);
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		if(z.getId() == null) return new Zwierze();
		return z;
	}
	
	public Zwierze getZwierzeByItsCKGZ(String ckgz){
		Zwierze z = new Zwierze();
		try {	
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze WHERE ckgz='" + ckgz + "';");
			while(rs.next()){
				Integer id = Integer.parseInt(rs.getString(1));
				String gatunek = rs.getString(2);
				String imie = rs.getString(3);
				String ckgz_new = rs.getString(4);
				z.setId(id);
				z.setGatunek(gatunek);
				z.setImie(imie);
				z.setCKGZ(ckgz);
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		if(z.getId() == null) return new Zwierze();
		return z;
	}
	
	//METODY BAZUJĄCE NA "STORAGE":
	//cel: pierwsze kroki projektu, testowanie obsługi JSONów
	
	@GET
	@Path("/id/{zwierzeId}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getZwierzeById(@PathParam("zwierzeId") Integer id){
		Zwierze z = storage.get(id);
		if(z == null){
			String returnInfo = "Nie znaleziono zwierzęcia o podanym ID (" + id + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = makeJsonPretty(z);
		return Response.status(200).entity(output).build();
	}

	@GET
	@Path("/gatunek/{zwierzeGatunek}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getZwierzeByGatunek(@PathParam("zwierzeGatunek") String gatunek){
		Zwierze z = null;
		for(Map.Entry<Integer, Zwierze> entry : storage.entrySet()){
			if(entry.getValue().getGatunek().equalsIgnoreCase(gatunek)){
				z = storage.get(entry.getKey());
				break;
			}
		}
		if(z == null){
			String returnInfo = "Nie znaleziono zwierzęcia podanego gatunku (" + gatunek + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = makeJsonPretty(z);
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/imie/{zwierzeImie}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getZwierzeByImie(@PathParam("zwierzeImie") String imie){
		Zwierze z = null;
		for(Map.Entry<Integer, Zwierze> entry : storage.entrySet()){
			if(entry.getValue().getImie().equalsIgnoreCase(imie)){
				z = storage.get(entry.getKey());
				break;
			}
		}
		if(z == null){
			String returnInfo = "Nie znaleziono zwierzęcia o podanym imieniu (" + imie + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = makeJsonPretty(z);
		return Response.status(200).entity(output).build();
	}

	@GET
	@Path("/ckgz/{zwierzeCKGZ}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getZwierzeByCKGZ(@PathParam("zwierzeCKGZ") String ckgz){
		Zwierze z = null;
		Map<Integer, Zwierze> zwierzMap = new HashMap<Integer, Zwierze>();
		for(Map.Entry<Integer, Zwierze> entry : storage.entrySet()){
			if(entry.getValue().getCKGZ().equalsIgnoreCase(ckgz)){
				z = storage.get(entry.getKey());
				zwierzMap.put(entry.getKey(),z);
			}
		}
		if(z == null){
			String returnInfo = "Nie znaleziono zwierzęcia o podanym stopniu zagrożenia (" + ckgz + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = makeJsonPretty(zwierzMap);
		return Response.status(200).entity(output).build();
	}

	@GET
	@Path("/wyraz/{zwierzeExpression}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getZwierzeByExpression(@PathParam("zwierzeExpression") String wyraz){
		Zwierze z = null;
		Map<Integer, Zwierze> zwierzMap = new HashMap<Integer, Zwierze>();
		for(Map.Entry<Integer, Zwierze> entry : storage.entrySet()){
			if(entry.getValue().getGatunek().toLowerCase().contains(wyraz.toLowerCase())){
				z = storage.get(entry.getKey());
				zwierzMap.put(entry.getKey(),z);
			}
			if(entry.getValue().getImie().toLowerCase().contains(wyraz.toLowerCase())){
				z = storage.get(entry.getKey());
				zwierzMap.put(entry.getKey(),z);
			}
		}
		if(z == null){
			String returnInfo = "Nie znaleziono zwierzęcia zawierającego podane wyrażenie (" + wyraz + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = makeJsonPretty(zwierzMap);
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/selectAllZwierze")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response selectAllZwierze(){
		String output = makeJsonPretty(storage);
		return Response.status(200).entity(output).build();
	}
	
	//DODATKOWE METODY:
	//cel: ładniejsze wyświetlanie JSONów
	
	public static String makeJsonPretty(Map<Integer, Zwierze> myMap){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(myMap);
		return jsonOutput;
	}
	
	public static String makeJsonPretty(Zwierze myZwierze){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(myZwierze);
		return jsonOutput;
	}
}
