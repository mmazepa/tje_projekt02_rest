package myPackage;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.sql.ResultSet;
import java.sql.SQLException;

@Path("/update")
public class UpdateZwierze {
	
	@EJB
	static ZwierzeManager zm = new ZwierzeManager();
	
	@GET
	@Path("/zwierze/{id}/{noweImie}")
	@Produces("text/html")
	public static Response addZwierze(
		@PathParam("id") Integer id,
		@PathParam("noweImie") String noweImie
	){
		Zwierze z = new Zwierze();
		int updated = 0;
		try {
			z = zm.getZwierzeByItsId(id);
			HsqlConnection.connectDB();
			String sql = "UPDATE Zwierze SET imie='" + noweImie + "' WHERE id=" + id + ";";
			updated = HsqlConnection.stmt.executeUpdate(sql);
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		
		String output = "<meta http-equiv='content-type' content='text/html;charset=utf-8' />";
		output = output.concat("<b>ZWIERZĘCIU O ID:</b> " + id + "<br/>");
		
		if(updated > 0){
			output = output.concat("<b>ZMIENIONO IMIĘ NA:</b> " + noweImie + "<br/>");
		}
		if(updated == 0){
			output = output.concat("<b>NIE ZMIENIONO IMIENIA NA:</b> " + noweImie + "<br/>");
			output = output.concat("<b>POWÓD:</b> Nie odnaleziono w bazie zwierzęcia o podanym id.<br/>");
		}
		
		output = output.concat("Sprawdź sam - <a href='../../../hsql/db'>sprawdź</a>.");
		return Response.status(200).entity(output).build();
	}
}
