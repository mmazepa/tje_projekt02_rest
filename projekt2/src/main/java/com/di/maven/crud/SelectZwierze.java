package myPackage;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.sql.ResultSet;
import java.sql.SQLException;

@Path("/selection")
public class SelectZwierze {
	
	@EJB
	static ZwierzeManager zm = new ZwierzeManager();
	
	@GET
	@Path("/zwierze_id/{id}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static Response selectZwierzeById(@PathParam("id") Integer id){
		Zwierze z = new Zwierze();
		z = zm.getZwierzeByItsId(id);
		if(z.getId() == null){
			String returnInfo = "Nie znaleziono zwierzęcia o podanym ID (" + id + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = zm.makeJsonPretty(z);
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/zwierze_gatunek/{gatunek}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static Response selectZwierzeByGatunek(@PathParam("gatunek") String gatunek){
		Zwierze z = new Zwierze();
		z = zm.getZwierzeByItsGatunek(gatunek);
		if(z.getId() == null){
			String returnInfo = "Nie znaleziono zwierzęcia podanego gatunku (" + gatunek + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = zm.makeJsonPretty(z);
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/zwierze_imie/{imie}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static Response selectZwierzeByImie(@PathParam("imie") String imie){
		Zwierze z = new Zwierze();
		z = zm.getZwierzeByItsImie(imie);
		if(z.getId() == null){
			String returnInfo = "Nie znaleziono zwierzęcia o podanym imieniu (" + imie + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = zm.makeJsonPretty(z);
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/zwierze_ckgz/{ckgz}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static Response selectZwierzeByCKGZ(@PathParam("ckgz") String ckgz){
		Zwierze z = new Zwierze();
		z = zm.getZwierzeByItsCKGZ(ckgz);
		if(z.getId() == null){
			String returnInfo = "Nie znaleziono zwierzęcia podanego stopnia zagrożenia (" + ckgz + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = zm.makeJsonPretty(z);
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/zwierze_wszystkie")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static Response selectAllZwierze(){
		Zwierze z = new Zwierze();
		Map<Integer, Zwierze> zwierzMap = new HashMap<Integer, Zwierze>();
		try {
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze;");
			while(rs.next()){
				Integer id = Integer.parseInt(rs.getString(1));
				String gatunek = rs.getString(2);
				String imie = rs.getString(3);
				String ckgz = rs.getString(4);
				z.setId(id);
				z.setGatunek(gatunek);
				z.setImie(imie);
				z.setCKGZ(ckgz);
				zwierzMap.put(id,new Zwierze(id,gatunek,imie,ckgz,null));
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		String output = ZwierzeManager.makeJsonPretty(zwierzMap);
		return Response.status(200).entity(output).build();
	}
}
