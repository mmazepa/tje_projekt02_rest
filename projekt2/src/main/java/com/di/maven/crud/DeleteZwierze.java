package myPackage;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.sql.ResultSet;
import java.sql.SQLException;

@Path("/delete")
public class DeleteZwierze {
	
	@EJB
	static ZwierzeManager zm = new ZwierzeManager();
	
	@GET
	@Path("/zwierze/{id}")
	@Produces("text/html")
	public static Response deleteZwierze(@PathParam("id") Integer id){
		
		int deleted = 0;
		try {
			HsqlConnection.connectDB();
			String sql = "DELETE FROM Zwierze WHERE id=" + id + ";";
			deleted = HsqlConnection.stmt.executeUpdate(sql);
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		
		String output = "<meta http-equiv='content-type' content='text/html;charset=utf-8' />";
		if(deleted > 0){
			output = output.concat("<b>USUNIĘTO ZWIERZA O ID:</b> " + id + ".<br/>");
		}
		if(deleted == 0){
			output = output.concat("<b>NIE USUNIĘTO ZWIERZA O ID:</b> " + id + ".<br/>");
			output = output.concat("<b>POWÓD:</b> Nie odnaleziono w bazie zwierzęcia o podanym id.<br/>");
		}
		output = output.concat("Sprawdź sam - <a href='../../hsql/db'>sprawdź</a>.");
		return Response.status(200).entity(output).build();
	}
}
