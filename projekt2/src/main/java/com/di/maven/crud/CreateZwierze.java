package myPackage;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.sql.ResultSet;
import java.sql.SQLException;

@Path("/create")
public class CreateZwierze {
	
	@EJB
	static ZwierzeManager zm = new ZwierzeManager();
	
	@GET
	@Path("/zwierze/{gatunek}/{imie}/{ckgz}")
	@Produces("text/html")
	public static Response addZwierze(
		@PathParam("gatunek") String gatunek,
		@PathParam("imie") String imie,
		@PathParam("ckgz") String ckgz
	){
		
		Zwierze z = new Zwierze(); 
		z.setGatunek(gatunek);
		z.setImie(imie);
		z.setCKGZ(ckgz);
		
		try {
			HsqlConnection.connectDB();
			String sql = "INSERT INTO Zwierze (gatunek,imie,ckgz) VALUES('" + z.getGatunek() + "','" + z.getImie() + "','" + z.getCKGZ() + "');";
			HsqlConnection.stmt.executeQuery(sql);
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		
		String output = "<meta http-equiv='content-type' content='text/html;charset=utf-8' />";
		output = output.concat("<b>DODANO ZWIERZA:</b> " + z.getGatunek() + " " + z.getImie() + ".<br/>");
		output = output.concat("Sprawdź sam - <a href='../../../../hsql/db'>sprawdź</a>.");
		return Response.status(200).entity(output).build();
	}
}
