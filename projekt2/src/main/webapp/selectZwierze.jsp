<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h4>Wyszukiwanie zwierzęcia w bazie:</h4>
<!-- szukanie po ID -->
<form onSubmit="return selectZwierzeById();" id="form">
	<b>GET:</b> projekt2/selection/zwierze_id/<br/>
	<input type="text" name="zwierzeId" id="zwierzeId" placeholder="wpisz id..." required>
	<input type="submit" id="submit" value="getById">
</form>
<!-- szukanie po gatunku -->
<form onSubmit="return selectZwierzeByGatunek();" id="form">
	<b>GET:</b> projekt2/selection/zwierze_gatunek/<br/>
	<input type="text" name="zwierzeGatunek" id="zwierzeGatunek" placeholder="wpisz gatunek..." required>
	<input type="submit" id="submit" value="getByGatunek">
</form>
<!-- szukanie po imieniu -->
<form onSubmit="return selectZwierzeByImie();" id="form">
	<b>GET:</b> projekt2/selection/zwierze_imie/<br/>
	<input type="text" name="zwierzeImie" id="zwierzeImie" placeholder="wpisz imie..." required>
	<input type="submit" id="submit" value="getByName">
</form>
<!-- szukanie po wpisie z czerwonek księgi -->
<form onSubmit="return selectZwierzeByCKGZ();" id="form">
	<b>GET:</b> projekt2/selection/zwierze_ckgz/<br/>
	<select id="zwierzeCKGZ" name="zwierzeCKGZ" required>
		<option>EX</option>
		<option>EW</option>
		<option>CR</option>
		<option>EN</option>
		<option>VU</option>
		<option>NT</option>
		<option>LC</option>
		<option>-</option>
	</select>
	<input type="submit" id="submit" value="getByCKGZ">
</form>
<p class="info">Znajdź zwierzę w bazie.</p>
<script src="static/javascript/simple.js"></script>
