<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h4>Wyszukiwanie wyrażenia:</h4>
<!-- wyrażenie nr 1: gatunek i imię -->
<form onSubmit="return zwierzByExpression();" id="form">
	<b>GET:</b> projekt2/zwierze/wyraz/
	<input type="text" id="zwierzeExpression" name="zwierzeExpression" placeholder="wpisz coś..." required>
	<input type="submit" id="submit" value="getZwierze">
</form>
<p class="info">
	Wyszukaj zwierząt po wyrażeniu, np. wyświetl wszystkie zwierzęta
	zawierające literę 'a' w gatunku lub imieniu.
</p>
<script src="static/javascript/simple.js"></script>
