<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h4>Wyszukiwanie po frazie:</h4>
<!-- fraza nr 1: gatunek -->
<form onSubmit="return zwierzByGatunek();" id="form">
	<b>GET:</b> projekt2/zwierze/gatunek/
	<input type="text" id="zwierzeGatunek" name="zwierzeGatunek" placeholder="wpisz gatunek..." required>
	<input type="submit" id="submit" value="getZwierze">
</form>
<!-- fraza nr 2: imie -->
<form onSubmit="return zwierzByImie();" id="form">
	<b>GET:</b> projekt2/zwierze/imie/
	<input type="text" id="zwierzeImie" name="zwierzeImie" placeholder="wpisz imię..." required>
	<input type="submit" id="submit" value="getZwierze">
</form>
<!-- fraza nr 3: ckgz -->
<form onSubmit="return zwierzByCKGZ();" id="form">
	<b>GET:</b> projekt2/zwierze/ckgz/
	<select id="zwierzeCKGZ" name="zwierzeCKGZ" required>
		<option>EX</option>
		<option>EW</option>
		<option>CR</option>
		<option>EN</option>
		<option>VU</option>
		<option>NT</option>
		<option>LC</option>
		<option>-</option>
	</select>
	<input type="submit" id="submit" value="getZwierze">
</form>
<p class="info">
	Wyszukaj zwierzęcia po frazie - gatunku, imieniu
	lub wybierz stopień zagrożenia z Czerwonej Księgi Gatunków Zagrożonych.
</p>
<script src="static/javascript/simple.js"></script>
