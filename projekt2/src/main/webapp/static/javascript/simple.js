//FUNKCJE UŻYTKOWE:
//cel: zmiana wyświetlanego stanu strony, czytanie z pliku

function displayGoodMorning(){
	allText = "<h3>Witam na stronie!</h3>\
				<p>Użyj przycisków z lewej strony w celu wykonania akcji.</p>";
	document.getElementById("mainBox").innerHTML = allText;
}

function readTextFile(file){
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function (){
        if(rawFile.readyState === 4){
            if(rawFile.status === 200 || rawFile.status == 0){
                var allText = rawFile.responseText;
                document.getElementById("mainBox").innerHTML = allText;
            }
        }
    }
    rawFile.send(null);
}


//FUNKCJE NAWIGACYJNE:
//cel: przekierowanie do konkretnych ścieżek

function goSomewhere(url){
	location.href=url;
}

function hello(){
	goSomewhere("projekt2/hello/" + document.getElementById("message").value);
	return false;
}

function zwierzById(){
	goSomewhere("projekt2/zwierze/id/" + document.getElementById("zwierzeId").value);
	return false;
}

function zwierzByGatunek(){
	goSomewhere("projekt2/zwierze/gatunek/" + document.getElementById("zwierzeGatunek").value);
	return false;
}

function zwierzByImie(){
	goSomewhere("projekt2/zwierze/imie/" + document.getElementById("zwierzeImie").value);
	return false;
}

function zwierzByCKGZ(){
	goSomewhere("projekt2/zwierze/ckgz/" + document.getElementById("zwierzeCKGZ").value);
	return false;
}

function zwierzByExpression(){
	goSomewhere("projekt2/zwierze/wyraz/" + document.getElementById("zwierzeExpression").value);
	return false;
}

function ksiegaById(){
	goSomewhere("projekt2/ksiega/id/" + document.getElementById("ksiegaId").value);
	return false;
}

function selectAllZwierze(){
	goSomewhere("projekt2/zwierze/selectAllZwierze");
	return false;
}

function selectAllKsiega(){
	goSomewhere("projekt2/ksiega/selectAllKsiega");
	return false;
}

function checkDB(){
	goSomewhere("projekt2/hsql/db");
	return false;
}

function resetDB(){
	goSomewhere("projekt2/hsql/reset");
	window.alert("Baza danych została zresetowana.");
	return false;
}

function addZwierze(){
	gatunek = document.getElementById("zwierzeGatunek").value;
	imie = document.getElementById("zwierzeImie").value;
	ckgz = document.getElementById("zwierzeCKGZ").value;
	goPath = "projekt2/create/zwierze/" + gatunek + "/" + imie + "/" + ckgz;
	goSomewhere(goPath);
	return false;
}

function deleteZwierze(){
	id = document.getElementById("zwierzeId").value;
	goPath = "projekt2/delete/zwierze/" + id;
	goSomewhere(goPath);
	return false;
}

function selectZwierzeById(){
	id = document.getElementById("zwierzeId").value;
	goPath = "projekt2/selection/zwierze_id/" + id;
	goSomewhere(goPath);
	return false;
}

function selectZwierzeByGatunek(){
	gatunek = document.getElementById("zwierzeGatunek").value;
	goPath = "projekt2/selection/zwierze_gatunek/" + gatunek;
	goSomewhere(goPath);
	return false;
}

function selectZwierzeByImie(){
	imie = document.getElementById("zwierzeImie").value;
	goPath = "projekt2/selection/zwierze_imie/" + imie;
	goSomewhere(goPath);
	return false;
}

function selectZwierzeByCKGZ(){
	ckgz = document.getElementById("zwierzeCKGZ").value;
	goPath = "projekt2/selection/zwierze_ckgz/" + ckgz;
	goSomewhere(goPath);
	return false;
}

function updateZwierze(){
	id = document.getElementById("zwierzeId").value;
	imie = document.getElementById("zwierzeImie").value;
	goPath = "projekt2/update/zwierze/" + id + "/" + imie;
	goSomewhere(goPath);
	return false;
}

function selectAllZwierze2(){
	goSomewhere("projekt2/selection/zwierze_wszystkie");
	return false;
}
