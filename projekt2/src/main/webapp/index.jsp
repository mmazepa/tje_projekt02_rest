<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>TJE: Project02</title>
		<meta type="text/html" charset="UTF-8" language="java" />
		<link type="text/css" rel="stylesheet" href="static/css/main.css" />
		<link type="text/css" rel="stylesheet" href="static/css/buttons.css" />
	</head>
	<body>
		<div id="logo"></div>
		<br/>
		<nav>
			<center>
				<p>
					<b>Zwierzęta w Polsce</b><br/>
					<i>Krótki przegląd</i>
				</p>
				<hr/>
				<button id="button0" onclick="displayGoodMorning();">Główny</button>
				<button id="button1" onclick="readTextFile('sendMessage.jsp');">sendMessage</button>
				<button id="button2" onclick="readTextFile('searchById.jsp');">byId</button>
				<button id="button3" onclick="readTextFile('searchByPhrase.jsp');">byPhrase</button>
				<button id="button4" onclick="readTextFile('searchByExpression.jsp');">byExpression</button>
				<button id="button5" onclick="readTextFile('selectAll.jsp');">displayAll</button>
			</center>
			<table align="center">
				<tr>
					<td class="td01">
						<button id="button6" onclick="checkDB();">checkDB</button>
						<button id="button7" onclick="resetDB();">resetDB</button>
						<button id="button8" onclick="readTextFile('selectZwierze.jsp');">getZwierze</button>
						<button id="button9" onclick="readTextFile('addZwierze.jsp');">addZwierze</button>
						<button id="button10" onclick="readTextFile('deleteZwierze.jsp');">delZwierze</button>
						<button id="button11" onclick="readTextFile('updateZwierze.jsp');">updateZwierze</button>
						<button id="button12" onclick="selectAllZwierze2();">selectAll</button>
					</td>
					<td id="mainBox" class="td02">
						<h3>Witam na stronie!</h3>
						<p>Użyj przycisków z lewej strony w celu wykonania akcji.</p>
					</td>
				</tr>
			</table>
		</nav>
		<br/>
		<script src="static/javascript/simple.js"></script>
	</body>
</html>
