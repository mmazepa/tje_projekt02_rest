<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h4>Wyszukiwanie po ID:</h4>
<!-- tabela nr 1: Zwierze -->
<form onSubmit="return zwierzById();" id="form">
	<b>GET:</b> projekt2/zwierze/id/
	<input type="number" name="zwierzeId" id="zwierzeId" pattern="[0-9]*" min="1" placeholder="wpisz id..." required>
	<input type="submit" id="submit" value="getZwierze">
</form>
<!-- tabela nr 2: CzerwonaKsiega -->
<form onSubmit="return ksiegaById();" id="form">
	<b>GET:</b> projekt2/ksiega/id/
	<input type="number" name="ksiegaId" id="ksiegaId" pattern="[0-9]*" min="1" placeholder="wpisz id..." required>
	<input type="submit" id="submit" value="getKsiega">
</form>
<p class="info">Wyszukaj zwierzęcia lub wpisu z księgi po ID.</p>
<script src="static/javascript/simple.js"></script>
