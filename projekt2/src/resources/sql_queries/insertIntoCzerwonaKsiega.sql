INSERT INTO CzerwonaKsiega (symbol,znaczeniePl,znaczenieAng)
VALUES
	('EX','Wymarłe','Extinct'),
	('EW','Wymarłe na wolności','Extinct in the wild'),
	('CR','Krytycznie zagrożone','Critically endangered'),
	('EN','Zagrożone','Endangered'),
	('VU','Narażone','Vulnerable'),
	('NT','Bliskie zagrożenia','Near threatened'),
	('LC','Najmniejszej troski','Least concern'),
	('-','Nie objęte ochroną','Not protected');
